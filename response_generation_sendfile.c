/*
 * response generation using sendfile():
 * make sure that after the last write operation the
 * packet is sent without corking, no unnecessary setsockopts
 */

#include <microhttpd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

static int
answer_to_connection (void *cls,
		      struct MHD_Connection *connection,
		      const char *url,
		      const char *method,
		      const char *version,
		      const char *upload_data,
		      size_t *upload_data_size,
		      void **con_cls)
{
	struct stat buf;
	struct MHD_Response *response;
	int ret;
	int fd;
	if ((0 != strcmp (method, MHD_HTTP_METHOD_GET)) &&
	    (0 != strcmp (method, MHD_HTTP_METHOD_HEAD)))
		return MHD_NO;
	if ((-1 == (fd = open ("a.txt", O_RDONLY))) || (0 != fstat (fd, &buf)))
	{
		if (fd != -1)
			close (fd);
		return 1; // error
	}
	response = MHD_create_response_from_fd64 (buf.st_size, fd);
	if (NULL == response)
	{
		if (0 != close (fd))
			abort ();
		return MHD_NO;
	}
	ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
	MHD_destroy_response (response);
	if (MHD_NO == ret)
		abort ();
	return ret;
}

int
main (void)
{
	struct MHD_Daemon *daemon;
	daemon =
	    MHD_start_daemon (MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD |
				  MHD_USE_ERROR_LOG,
			      8888,
			      NULL,
			      NULL,
			      &answer_to_connection,
			      NULL,
			      NULL,
			      MHD_OPTION_END);
	if (NULL == daemon)
		return 1;
	(void) getchar ();
	MHD_stop_daemon (daemon);
	return 0;
}

/* EOF response_generation_sendfile.c  */
