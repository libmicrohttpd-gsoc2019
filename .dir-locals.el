((nil . ((indent-tabs-mode . t)
         (tab-width . 8)
         (fill-column . 80)))
 ;; Warn about spaces used for indentation:
 (haskell-mode . ((eval . (highlight-regexp "^ *"))))
 (c-mode . ((c-file-style . "BSD")
	    (eval add-hook 'before-save-hook #'clang-format-buffer nil t)))
  (c++-mode . ((c-file-style . "BSD")
	       (eval add-hook 'before-save-hook #'clang-format-buffer nil t)))
 (java-mode . ((c-file-style . "BSD")))
 ("src/imported"
  . ((nil . ((change-log-default-name . "ChangeLog.local"))))))
