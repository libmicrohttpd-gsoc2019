#include <microhttpd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static int
answer_to_connection (void *cls,
		      struct MHD_Connection *connection,
		      const char *url,
		      const char *method,
		      const char *version,
		      const char *upload_data,
		      size_t *upload_data_size,
		      void **con_cls)
{
	const char *body = "<html><body>\
			    <p>\
			    Hello, ick bins eins HTML body!<br>\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    Ia! Ia! Cthulhu fhtagn! Ph'nglui mglw'nfah Cthulhu R'lyeh wgah'nagl fhtagn!\
			    </p>\
			    </body></html>";
	int ret;
	struct MHD_Response *response =
	    MHD_create_response_from_buffer (strlen (body),
					     (void *) body,
					     MHD_RESPMEM_PERSISTENT);
	ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
	MHD_destroy_response (response);
	return ret;
}

int
main ()
{
	struct MHD_Daemon *daemon =
	    MHD_start_daemon (MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD,
			      8888,
			      NULL,
			      NULL,
			      &answer_to_connection,
			      NULL,
			      MHD_OPTION_END);
	if (NULL == daemon)
		return 1;
	(void) getchar ();
	MHD_stop_daemon (daemon);
	return 0;
}

/* EOF mrg.c  */
