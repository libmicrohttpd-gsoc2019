#include <microhttpd.h>
#include <stdlib.h>
#include <stdio.h>

static ssize_t
crc (void *cls, uint64_t pos, char *buf, size_t size_max)
{
	if (0 == size_max)
		return 0;
	if (0 == rand () % 1024 * 1024)
		return MHD_CONTENT_READER_END_OF_STREAM;
	*buf = 'b';
	return 1;
}


static int
answer_to_connection (void *cls,
		      struct MHD_Connection *connection,
		      const char *url,
		      const char *method,
		      const char *version,
		      const char *upload_data,
		      size_t *upload_data_size,
		      void **con_cls)
{
	int ret;
	struct MHD_Response *response =
	    MHD_create_response_from_callback (MHD_SIZE_UNKNOWN,
					       1024,
					       &crc,
					       NULL,
					       NULL);
	ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
	MHD_destroy_response (response);
	return ret;
}

int
main ()
{
	struct MHD_Daemon *daemon =
	    MHD_start_daemon (MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD,
			      8888,
			      NULL,
			      NULL,
			      &answer_to_connection,
			      NULL,
			      MHD_OPTION_END);
	if (NULL == daemon)
		return 1;
	(void) getchar ();
	MHD_stop_daemon (daemon);
	return 0;
}

/* EOF inc.c  */
