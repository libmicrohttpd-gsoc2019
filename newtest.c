#include <microhttpd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 *
 */

int
main (void)
{
	struct MHD_Daemon *daemon;
	// initialize the daemon
	daemon =
	    MHD_start_daemon (MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD |
				  MHD_USE_ERROR_LOG,
			      80,
			      NULL,
			      NULL,
			      &answer_to_connection,
			      NULL,
			      NULL,
			      MHD_OPTION_END);
	// call the daemon
	if (NULL == daemon)
		return 1;
	(void) getchar ();
	// stop the daemon (never reached?)
	MHD_stop_daemon (daemon);
	return 0;
}
