PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig:$HOME/code/dev/prefix/lib/pkgconfig:$HOME/opt/lib/pkgconfig
CFLAGS=		`pkg-config --cflags libmicrohttpd` -Wl,-R/usr/local/lib -Wl,-R/home/ng0/opt/lib -Wl,-R/home/ng0/code/dev/prefix/lib
LDFLAGS=	`pkg-config --libs libmicrohttpd` -Wl,-R/usr/local/lib -Wl,-R/home/ng0/opt/lib -Wl,-R/home/ng0/code/dev/prefix/lib

all: inc trg response_generation_sendfile mrg

inc:
	mkdir build || true
	clang $(CFLAGS) inc.c $(LDFLAGS) -o build/inc

trg:
	mkdir build || true
	clang $(CFLAGS) trg.c $(LDFLAGS) -o build/trg

response_generation_sendfile:
	mkdir build || true
	clang $(CFLAGS) response_generation_sendfile.c $(LDFLAGS) -o build/response_generation_sendfile

mrg:
	mkdir build || true
	clang $(CFLAGS) mrg.c $(LDFLAGS) -o build/mrg

clean:
	rm -rf build
