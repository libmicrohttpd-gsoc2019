/*
 * starting point: NetBSD's <netinet/tcp.h>
 */
enum MHD_socket_options
{
	MHD_TCP_NODELAY = 1, /* don't delay send to coalesce packets */
	MHD_TCP_MAXSEG = 2, /* set maximum segment size */
	MHD_TCP_KEEPIDLE = 3,
	MHD_TCP_NOPUSH = 4, /* reserved for FreeBSD compat */
	MHD_TCP_KEEPINTVL = 5,
	MHD_TCP_KEEPCNT = 6,
	MHD_TCP_KEEPINIT = 7,
	MHD_TCP_NOOPT = 8, /* reserved for FreeBSD compat */
	MHD_TCP_INFO = 9, /* retrieve tcp_info structure */
	MHD_TCP_MD5SIG = 10, /* use MD5 digests (RFC2385) */
	MHD_TCP_CONGCTL = 11, /* selected congestion control */
	MHD_TCPI_OPT_TIMESTAMPS = 12,
	MHD_TCPI_OPT_SACK = 13,
	MHD_TCPI_OPT_WSCALE = 14,
	MHD_TCPI_OPT_ECN = 15,
	MHD_TCPI_OPT_TOE = 16
};

/*
 * prototype for setsocket etc wrapper
 */
void
MHD_socketoption (struct MHD_Connnection *connection,
		  char *buffer,
		  size_t buffersize,
		  enum MHD_socket_options);

/* EOF wrapperfunc.c  */
